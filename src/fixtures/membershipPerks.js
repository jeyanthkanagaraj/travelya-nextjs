import { faPlane, faBell, faDollarSign, faLongArrowAltDown } from '@fortawesome/free-solid-svg-icons'

export const membershipPerks = [
    {
        'id': 0,
        'icon': faPlane,
        'description': 'More flight deals'
    },
    {
        'id': 1,
        'icon': faBell,
        'description': 'Deal notifications'
    },
    {
        'id': 2,
        'icon': faLongArrowAltDown,
        'description': 'Lowest price guarentee'
    },
    {
        'id': 3,
        'icon': faDollarSign,
        'description': '6 month money back'
    }
]

export const user = {
    firstName: 'Sara',
    lastName: 'Smith',
    userImage: '/female.jpg',
    description: 'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century'
}
