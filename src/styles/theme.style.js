import { makeMediaQuery } from 'lib/makeMediaQuery'

export const palette = {
    primary: { main: '#F26522' },
    secondary: '#5D9199',
    black: '#000',
    gray: '#88959E',
    white: '#FFF',
    blue: '#0366d6'
}
export const fonts = {
    family: {
        body: 'Gotham',
        heading: 'Gotham',
    }
}
export const ruler = {
    header: {
        width: '130rem',
        mobileHeight: '6.5rem',
    },
    content: { width: '110rem' }
}
export const zIndex = {
    bringForward: 1,
    sendBack: -1
}

export const container = {
    flexCentered: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    padded: {
        padding: '2rem',
    }
}

export const border = {
    gray: `.1rem solid ${palette.gray}`,
}

export const mq = makeMediaQuery()
