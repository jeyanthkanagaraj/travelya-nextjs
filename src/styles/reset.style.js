import { css, Global } from '@emotion/core'
import React from 'react'

const styles = theme => css`
    html {
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        box-sizing: border-box;
        font-size: 10px;
        scroll-behavior: smooth;
        @media only screen and (prefers-reduced-motion: reduce) {
            scroll-behavior: auto;
        }
    }

    *,
    *::before,
    *::after {
        box-sizing: inherit;
    }

    body {
        font-family: ${theme.fonts.family.body};
        margin: 0 auto;
        padding: 0;
    }

    a,
    img {
        display: block;
    }

    img {
        max-width: 100%;
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        font-family: ${theme.fonts.family.heading};
        margin: 0;
    }
`

/**
 *
 * @type {React.Component}
 * @name CSSReset
 * @description global style reset for the app
 */
export const CSSReset = () => <Global styles={styles} />
