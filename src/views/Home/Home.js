import { useRouter } from 'next/router'
import React from 'react'
import FlightDeals from 'views/FlightDeals/FlightDeals'

export const Home = () => {
    // TODO: auth functionality
    const loggedIn = true
    const router = useRouter()

    return loggedIn ? <FlightDeals /> : router.push('/signin')
}
