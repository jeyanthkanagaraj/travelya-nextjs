import styled from '@emotion/styled'
import React from 'react'
import FlightCard from 'components/FlightCard/FlightCard'
import flightDeals from 'fixtures/flightDeals.json'

const FlightDeals = () => (
    <Container>
        <Title>Flight Deals</Title>
        <Filters>
            <FilterButton active>All</FilterButton>
            <FilterButton>Roundtrip</FilterButton>
            <FilterButton>One Way</FilterButton>
            <FilterButton>Premium</FilterButton>
        </Filters>
        <Results>
            {flightDeals.map((flight, i) => <FlightCard key={i} {...flight} />)}
        </Results>
    </Container>
)

export default FlightDeals

const Container = styled.div(({ theme }) => {
    return {
        margin: '0 auto',
        maxWidth: theme.ruler.content.width,
        '& > *': {
            color: theme.palette.gray,
        },
        [theme.mq.medium]: {
            padding: '2rem 0',
        }
    }
})

const Title = styled.h1(({ theme }) => ({
    position: 'fixed',
    top: 0,
    width: '100%',
    fontWeight: 'bold',
    textTransform: 'uppercase',
    fontSize: '2rem',
    textAlign: 'center',
    margin: '2rem 0',
    zIndex: theme.zIndex.bringForward,
    [theme.mq.medium]: {
        position: 'static',
        marginBottom: '4rem',
        top: 'auto',
    }
}))

const Filters = styled.div(({ theme }) => ({
    display: 'none',
    [theme.mq.medium]: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: '3rem',
        padding: '1rem 0'
    }
}))

const FilterButton = styled.button(({ theme, active }) => ({
    border: active ? `.2rem solid ${theme.palette.primary.main}` : 'none',
    boxShadow: active ? 'none' : '0 .1rem .5rem rgba(0,0,0,.15)',
    borderRadius: '5rem',
    padding: '1rem 3rem',
    margin: '0 1rem',
    color: active ? theme.palette.primary.main : theme.palette.gray,
    background: 'none',
    fontWeight: 'bold',
    fontSize: '1.4rem',
    cursor: 'pointer'
}))

const Results = styled.section(({ theme }) => ({
    width: '100%',
    fontSize: '1.5rem',
    [theme.mq.medium]: {
        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap',
    },
    [theme.mq.large]: {
        padding: '2rem',
    }
}))
