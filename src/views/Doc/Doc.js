import Document, {
    Html,
    Head,
    Main,
    NextScript,
} from 'next/document'

export class Doc extends Document {
    render () {
        return (
            <Html lang="en">
                <Head>
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <link rel="stylesheet" href="/fonts/fonts.css" />
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        )
    }
}
