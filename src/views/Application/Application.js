import 'core-js/stable'
import 'regenerator-runtime/runtime'
import { ThemeProvider } from 'emotion-theming'
import App from 'next/app'
import Router from 'next/router'
import React from 'react'
import Layout from 'components/Layout/Layout'
import { CSSReset } from 'styles/reset.style'
import * as theme from 'styles/theme.style'

const handleRouteChange = () => window.scrollTo(0,0)

export class Application extends App {
    static async getInitialProps ({ ctx, Component }) {
        let pageProps = {}
        if (Component.getInitialProps) {
            pageProps = await Component.getInitialProps({ ...ctx })
        }
        return { pageProps }
    }
    componentDidMount () {
        Router.events.on('routeChangeComplete', handleRouteChange)
    }
    componentWillUnmount () {
        Router.events.off('routeChangeComplete',handleRouteChange)
    }

    render () {
        const { Component, pageProps, router } = this.props

        if (['/signin', '/signup'].includes(router.pathname)) {
            return (
                <ThemeProvider theme={theme}>
                    <CSSReset />
                    <Component {...pageProps} />
                </ThemeProvider>
            )
        }

        return (
            <ThemeProvider theme={theme}>
                <CSSReset />
                <Layout>
                    <Component {...pageProps} />
                </Layout>
            </ThemeProvider>
        )
    }
}
