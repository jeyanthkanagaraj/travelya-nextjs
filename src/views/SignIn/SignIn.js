import React from 'react'
import AuthForm from 'components/AuthForm/AuthForm'

export const SignIn = () => {
    return (
        <AuthForm isLogin onSubmit={() => {}} />
    )
}
