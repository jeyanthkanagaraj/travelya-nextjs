import styled from '@emotion/styled'
import React, { useState } from 'react'
import ContactForm from 'components/ContactForm/ContactForm'
import SubscriptionDialog from 'components/SubscriptionDialog/SubscriptionDialog'

const Support = () => {
    const [IsModalOpen, setIsModalOpen] = useState(false)
    const handleSubmit = (e) => {
        e.preventDefault()
        setIsModalOpen(true)
    }
    return (
        <Container>
            <Title>Support Center</Title>
            <ContactForm handleSubmit={handleSubmit} />
            {IsModalOpen ? <SubscriptionDialog setIsModalOpen={setIsModalOpen} /> : null}
        </Container>
    )
}

export { Support }

const Container = styled.div(({ theme }) => {
    return {
        margin: '0 auto',
        maxWidth: theme.ruler.content.width,
        '& > *': {
            color: theme.palette.gray,
        },
        [theme.mq.medium]: {
            padding: '2rem 0',
            width: '65%'
        }
    }
})

const Title = styled.h1(({ theme }) => ({
    position: 'fixed',
    top: 0,
    width: '100%',
    fontWeight: 'bold',
    textTransform: 'capitalize',
    fontSize: '2rem',
    textAlign: 'center',
    margin: '2rem 0',
    color: theme.palette.primary.main,
    zIndex: theme.zIndex.bringForward,
    [theme.mq.medium]: {
        position: 'static',
        marginBottom: '4rem',
        top: 'auto',
        textAlign: 'left',
    }
}))

const Results = styled.section(({ theme }) => ({
    width: '100%',
    fontSize: '1.5rem',
    [theme.mq.medium]: {
        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap',
    },
    [theme.mq.large]: {
        padding: '2rem',
    }
}))
