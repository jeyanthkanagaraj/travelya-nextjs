import styled from '@emotion/styled'
import React, { Fragment } from 'react'
import IconHamburger from 'components/Icons/IconHamburger'
import NavDesktop from 'components/NavDesktop/NavDesktop'
// import NavMobile from 'components/NavMobile/NavMobile' -- TODO: sidebar component

const Layout = ({ children }) => {
    return (
        <Fragment>
            <Header>
                <NavButton>
                    <IconHamburger />
                </NavButton>
                <NavDesktop />
            </Header>
            <Main>
                {children}
            </Main>
        </Fragment>
    )
}

export default Layout

const Main = styled.main(({ theme }) => ({
    minHeight: `calc(100vh - ${theme.ruler.header.mobileHeight})`,
    marginTop: theme.ruler.header.mobileHeight,
    [theme.mq.medium]: {
        marginTop: 0,
        minHeight: '100vh',
    }
}))

const Header = styled.header(({ theme }) => ({
    position: 'fixed',
    top: 0,
    width: '100%',
    height: theme.ruler.header.mobileHeight,
    zIndex: theme.zIndex.bringForward,
    background: theme.palette.white,
    [theme.mq.medium]: {
        position: 'static',
        top: 'auto',
        height: 'auto',
    }
}))

const NavButton = styled.button(({ theme }) => ({
    margin: 0,
    padding: '.5rem',
    position: 'absolute',
    left: '1rem',
    top: `calc((${theme.ruler.header.mobileHeight} - 3rem) / 2)`,
    height: '3rem',
    width: '3rem',
    border: 'none',
    background: 'none',
    cursor: 'pointer',
    '& > svg': {
        fill: theme.palette.gray,
        height: '100%',
        width: 'auto',
        verticalAlign: 'middle'
    },
    [theme.mq.medium]: {
        display: 'none',
    }
}))
