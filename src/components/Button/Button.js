import styled from '@emotion/styled'

const Button = styled.button(({ theme, primary }) => {
    let background = theme.palette.white
    let color = theme.palette.black
    let border = theme.border.gray
    let fontWeight = 'normal'

    if (primary) {
        background = theme.palette.primary.main
        color = theme.palette.white
        border = `.1rem solid ${theme.palette.primary.main}`
        fontWeight = 'bold'
    }

    return {
        fontWeight,
        background,
        border,
        borderRadius: '1rem',
        color,
        width: '100%',
        marginBottom: '1rem',
        padding: '1rem',
        fontFamily: 'inherit',
        textTransform: 'uppercase',
        cursor: 'pointer',
    }
})

export default Button
