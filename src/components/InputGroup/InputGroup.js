import styled from '@emotion/styled'
import React, { useState, useRef } from 'react'

const InputGroup = ({ label, inputId, ...props }) => {
    const [active, setActive] = useState(false)
    const inputRef = useRef(null)
    const onBlur = () => {
        !inputRef?.current?.value && setActive(false)
    }

    return (
        <Wrapper>
            <Label htmlFor={inputId} active={active}>
                {label}
            </Label>
            <Input
                ref={inputRef}
                onFocus={() => setActive(true)}
                onBlur={onBlur}
                id={inputId}
                {...props} />
        </Wrapper>
    )
}

export default InputGroup

const Wrapper = styled.div(() => ({
    position: 'relative',
    width: '100%',
}))

const Label = styled.label(({ theme, active }) => ({
    color: theme.palette.gray,
    textTransform: 'uppercase',
    position: 'absolute',
    top: active ? '-2rem' : '.5rem',
    fontSize: active ? '1rem' : '1.3rem',
    transition: 'all .2s'
}))

const Input = styled.input(({ theme }) => ({
    lineHeight: 2,
    border: 'none',
    borderBottom: theme.border.gray,
    fontFamily: 'inherit',
    width: '100%',
    marginBottom: '3rem',
    padding: '0 .5rem'
}))
