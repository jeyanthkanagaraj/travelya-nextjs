import styled from '@emotion/styled'
import React from 'react'
import { PaymentInputsWrapper, usePaymentInputs } from 'react-payment-inputs'
import { css } from 'styled-components'
import Button from 'components/Button/Button'
import images from 'react-payment-inputs/images'

const TrialForm = ({ cardNumber, setCVC, setExpiryDate, expiryDate, cvc, setCardNumber }) => {

    const { wrapperProps, getCardImageProps, getCardNumberProps, getExpiryDateProps, getCVCProps } = usePaymentInputs()
    return (
        <Container>
            <PaymentInputsWrapper {...wrapperProps} styles={CustomStyle}>
                <svg {...getCardImageProps({ images })} />
                <input {...getCardNumberProps()} />
                <input {...getExpiryDateProps()} />
                <input {...getCVCProps()} />
            </PaymentInputsWrapper>
            <Link href="#">promo code?</Link>
            <ButtonCover>
                <Button primary>Start Free Trial</Button>
            </ButtonCover>
            <Text>By signing up, you agree to the <Link href="#">Terms of Use</Link> and <Link href="#">Refund Policy</Link></Text>
        </Container>
    )
}

export default TrialForm

const Container = styled.main(() => ({
}))

const Link = styled.a(({ theme }) => ({
    color: theme.palette.blue,
    textDecoration: 'none',
    textAlign: 'right',
    marginTop: '-5px'
}))

const ButtonCover = styled.div(({ theme }) => ({
    width: '50%',
    margin: '25px auto',
    [theme.mq.medium]: {
        width: '50%',
    }
}))

const Text = styled.p(() => ({
    textAlign: 'center',
    width: '100%',
    '& a': {
        display: 'inline',
        marginTop: 0,
        textAlign: 'center'
    }
}))

//Styles for Payment Form
const CustomStyle = {
    fieldWrapper: {
        base: css`
            margin-bottom: 1rem;
            width: 100%
        `
    },
    inputWrapper: {
        base: css`
            border: 0;
            box-shadow: none;
            border-bottom: 1px solid #ccc;

        `,
        focused: css`
            border-color: unset;
            box-shadow: unset;
        `
    },
    input: {
        base: css`
            color: #88959E;
            font-size: 15px;
        `,
        cardNumber: css`
            width: 32rem;
        `,
        expiryDate: css`
            width: 10rem;
        `,
        cvc: css`
            width: 5rem;
        `
    },
}
