import styled from '@emotion/styled'
import React, { useState } from 'react'
import MemberCard from 'components/MemberCard/MemberCard'
import PerksList from 'components/PerksList/PerksList'
import TrialForm from 'components/TrialForm/TrialForm'
import { membershipPerks, user } from 'fixtures/membershipPerks'

const SubscriptionDialog = ({ setIsModalOpen }) => {
    const [CardNumber, setCardNumber] = useState(null)
    const [ExpiryDate, setExpiryDate] = useState('')
    const [CVC, setCVC] = useState('')

    return (
        <Modal>
            <Container>
                <Close onClick={() => setIsModalOpen(false)}>x</Close>
                <Title>Membership Perks</Title>
                <SubTitle>Free for 14 days | Cancel at any time</SubTitle>
                <Lists>
                    {membershipPerks.map(member => <PerksList key={member.id} {...member} />)}
                </Lists>
                <TrialForm
                    setCardNumber={setCardNumber}
                    cardNumber={CardNumber}
                    expiryDate={ExpiryDate}
                    cvc={CVC}
                    setExpiryDate={setExpiryDate}
                    setCVC={setCVC} />
                <MemberCard user={user} />
            </Container>
        </Modal>
    )
}

export default SubscriptionDialog

const Modal = styled.main(({ theme }) => ({
    ...theme.container.flexCentered,
    margin: 0,
    padding: 0,
    position: 'fixed',
    right: 0,
    top: 0,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
}))

const Container = styled.div(({ theme }) => ({
    boxShadow: '0 1px 6px rgba(0,0,0,0.1), 0 0 0 0px rgba(0,0,0,0.1)',
    borderRadius: '3px',
    width: '358px',
    backgroundColor: '#fff',
    padding: 9,
    position: 'relative',
    [theme.mq.medium]: {
        width: '600px',
        padding: 50
    }
}))

const Title = styled.h3(({ theme }) => ({
    color: theme.palette.primary.main,
    textAlign: 'center',
    textTransform: 'uppercase',
    fontSize: 20
}))

const SubTitle = styled.p(() => ({
    textAlign: 'center',
    marginTop: 0
}))

const Lists = styled.div(({ theme }) => ({
    width: '65%',
    margin: '30px auto',
    [theme.mq.medium]: {
        width: '50%',
    }
}))

const Close = styled.p(() => ({
    position: 'absolute',
    top: 0,
    right: 16,
    fontSize: 16,
    cursor: 'pointer'
}))
