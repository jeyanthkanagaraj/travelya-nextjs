import styled from '@emotion/styled'
import Link from 'next/link'
import React from 'react'

/**
 * @description Custom Link component for the application built on top of
 * Next's Link component. Ensures direct child is always an anchor tag.
 * @param {Object} props
 */
const CustomLink = ({ children, ...extra }) => (
    <Link {...extra}>
        <Anchor>{children}</Anchor>
    </Link>
)

export default CustomLink

const Anchor = styled.a(() => ({
    textDecoration: 'none',
    cursor: 'pointer',
    '&:active, &:link, &:visited': {
        textDecoration: 'none',
    },
}))
