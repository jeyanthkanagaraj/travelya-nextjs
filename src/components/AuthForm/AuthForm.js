import styled from '@emotion/styled'
import React, { useState } from 'react'
import Button from 'components/Button/Button'
import IconFacebook from 'components/Icons/IconFacebook'
import IconGoogle from 'components/Icons/IconGoogle'
import IconLogo from 'components/Icons/IconLogo'
import InputGroup from 'components/InputGroup/InputGroup'
import Link from 'components/Link/Link'

const AuthForm = ({ isLogin, onSubmit }) => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const redirectTo = isLogin ? '/signup' : '/signin'

    const handleSubmit = e => {
        e.preventDefault()
        return onSubmit(email, password)
    }

    return (
        <Container>
            <IconLogo />
            <Form onSubmit={handleSubmit}>
                <InputGroup
                    label="Email Address"
                    inputId="email"
                    type="email"
                    onChange={e => setEmail(e.target.value)}
                    value={email} />
                <InputGroup
                    label="Password"
                    inputId="password"
                    type="password"
                    onChange={e => setPassword(e.target.value)}
                    value={password} />
                <Button type="submit" primary>
                    {isLogin ? 'Sign In' : 'Sign Up'}
                </Button>
            </Form>
            <CallToAction>
                {isLogin ? 'New to Travelaya?' : 'Already a travel lover?'}
            </CallToAction>
            <Link href={redirectTo} as={redirectTo}>
                <LinkText>
                    {isLogin ? 'Sign Up' : 'Sign In'}
                </LinkText>
            </Link>
            <Button>
                <ButtonText>
                    <IconGoogle height="2rem" />
                    Log in with Google
                </ButtonText>
            </Button>
            <Button>
                <ButtonText>
                    <IconFacebook height="2rem" />
                    Log in with Facebook
                </ButtonText>
            </Button>
        </Container>
    )
}

AuthForm.defaultProps = {
    isLogin: false,
    onSubmit: () => null,
}

export default AuthForm

const Container = styled.main(({ theme }) => ({
    ...theme.container.flexCentered,
    ...theme.container.padded,
    justifyContent: 'center',
    minHeight: '100vh',
    maxWidth: '40rem',
    margin: '0 auto',
    color: theme.palette.gray,
}))

const Form = styled.form(({ theme }) => ({
    ...theme.container.flexCentered,
    width: '100%',
    margin: '3rem 0'
}))

const CallToAction = styled.p(() => ({
    textAlign: 'center',
    fontSize: '1.5rem'
}))

const LinkText = styled.div(({ theme }) => ({
    textAlign: 'center',
    textTransform: 'uppercase',
    marginBottom: '4rem',
    borderBottom: '.1rem solid',
    color: theme.palette.primary.main,
    fontSize: '1.5rem',
}))

const ButtonText = styled.div(() => ({
    display: 'flex',
    fontSize: '1.2rem',
    alignItems: 'center',
    justifyContent: 'center',
    '& svg': {
        marginRight: '1rem',
    }
}))
