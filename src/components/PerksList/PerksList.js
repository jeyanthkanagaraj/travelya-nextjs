import styled from '@emotion/styled'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const PerksList = ({ icon, description }) => {
    return (
        <Container icon={icon}>
            <FontAwesomeIcon icon={icon} size="sm" />
            <p>{description}</p>
        </Container>

    )
}

export default PerksList

const Container = styled.main(({ theme, icon }) => ({
    display: 'flex',
    alignItems: 'center',
    '& p': {
        fontSize: 15,
        marginLeft: 10
    },
    '& svg': {
        color: theme.palette.primary.main,
        width: '18px !important',
        height: 18,
        transform: icon.iconName === 'plane' ? 'rotate(315deg)' : 0
    }
}))
