import styled from '@emotion/styled'
import React from 'react'

const FlightCard = ({ location, start, end, premium, type, price, image }) => {
    const imageUrl = image?.url || 'https://via.placeholder.com/400X300'

    return (
        <Box>
            <Figure>
                <Image src={imageUrl} alt={location} />
            </Figure>
            <TopContent>
                <div><b>{location}</b></div>
                <div>{start} - {end}</div>
            </TopContent>
            <BottomContent>
                <div>{type}</div>
                <div><b>${price}</b></div>
                {premium && (<Badge>Premium</Badge>)}
            </BottomContent>
        </Box>
    )
}

export default FlightCard

const Box = styled.div(({ theme }) => ({
    borderRadius: '.5rem',
    display: 'block',
    textTransform: 'uppercase',
    position: 'relative',
    margin: '0 0 2rem',
    cursor: 'pointer',
    [theme.mq.medium]: {
        width: '30%',
        margin: '1rem .5rem',
        position: 'static',
        boxShadow: '0 .1rem .5rem rgba(0,0,0,.15)',
    },
    [theme.mq.large]: {
        margin: '1.2rem',
    }
}))

const Figure = styled.figure(({ theme }) => ({
    overflow: 'hidden',
    width: '100%',
    height: '20rem',
    margin: 0,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    [theme.mq.medium]: {
        height: 'auto'
    }
}))

const Image = styled.img(({ theme }) => ({
    width: '100%',
    height: 'auto',
    [theme.mq.medium]: {
        borderTopLeftRadius: '.5rem',
        borderTopRightRadius: '.5rem',
    }
}))

const TopContent = styled.div(({ theme }) => ({
    padding: '1rem',
    color: theme.palette.gray,
    textTransform: 'uppercase',
}))

const BottomContent = styled.div(({ theme }) => ({
    textAlign: 'right',
    position: 'absolute',
    right: '1rem',
    bottom: '1rem',
    [theme.mq.medium]: {
        padding: '1rem',
        position: 'relative',
        right: 'auto',
        bottom: 'auto',
    }
}))

const Badge = styled.div(({ theme }) => ({
    display: 'none',
    visibility: 'hidden',
    background: theme.palette.primary.main,
    color: theme.palette.white,
    borderRadius: '3rem',
    position: 'absolute',
    left: '1rem',
    bottom: '1rem',
    padding: '.8rem 3rem',
    fontSize: '1rem',
    fontWeight: 'bold',
    [theme.mq.medium]: {
        display: 'block',
        visibility: 'visible'
    }
}))
