import styled from '@emotion/styled'
import React, { useState } from 'react'
import Button from 'components/Button/Button'
import InputGroup from 'components/InputGroup/InputGroup'

const ContactForm = ({ handleSubmit }) => {
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('')
    const [subject, setSubject] = useState('')
    const [details, setDetails] = useState('')

    return (
        <Container>
            <Form onSubmit={(e) => handleSubmit(e)}>
                <InputFlex>
                    <InputGroup
                        label="First Name"
                        inputId="firstName"
                        type="text"
                        onChange={e => setFirstName(e.target.value)}
                        value={firstName} />
                    <InputGroup
                        label="Last Name"
                        inputId="lastName"
                        type="text"
                        onChange={e => setLastName(e.target.value)}
                        value={lastName} />
                </InputFlex>
                <InputFlex>
                    <InputGroup
                        label="Email Address"
                        inputId="email"
                        type="email"
                        onChange={e => setEmail(e.target.value)}
                        value={email} />
                    <InputGroup
                        label="Subject"
                        inputId="subject"
                        type="text"
                        onChange={e => setSubject(e.target.value)}
                        value={subject} />
                </InputFlex>
                <TextArea
                    type="text"
                    onChange={e => setDetails(e.target.value)}
                    value={details}
                    rows="14" />
                <ButtonWrapper>
                    <Button type="submit" primary >
                        Submit
                    </Button>
                </ButtonWrapper>
            </Form>
        </Container>
    )
}

export default ContactForm

const Container = styled.main(({ theme }) => ({
    ...theme.container.flexCentered,
    justifyContent: 'center',
    maxWidth: '100%',
    margin: '0 auto',
    color: theme.palette.gray,
}))

const Form = styled.form(({ theme }) => ({
    ...theme.container.flexCentered,
    width: '80%',
    margin: '3rem 0',
    [theme.mq.medium]: {
        width: '100%'
    }
}))

const InputFlex = styled.div(({ theme }) => {
    return {
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'column',
        [theme.mq.medium]: {
            flexDirection: 'row'
        },
        '& div': {
            [theme.mq.medium]: {
                marginRight: '5%',
                '&:last-child': {
                    marginRight: 0
                },
            }
        }
    }
})

const TextArea = styled.textarea(({ theme }) => ({
    width: '100%',
    border: theme.border.gray,
    borderRadius: '5px',
    boxShadow: '0 0 5px rgba(0, 0, 0, 0.3)',
    marginTop: '15px',
    marginBottom: '30px'
}))

const ButtonWrapper = styled.div(() => ({
    width: '25%',
    margin: '0 auto',
}))
