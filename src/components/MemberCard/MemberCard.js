import styled from '@emotion/styled'
import React from 'react'

const MemeberCard = ({ user }) => {
    return (
        <Container>
            <UserDetails>
                <UserImage src={user.userImage} alt={`${user.firstName} ${user.LastName}`} />
                <UserName>{user.firstName} {user.lastName}</UserName>
            </UserDetails>
            <UserDescription>{user.description}</UserDescription>
        </Container>
    )
}

export default MemeberCard

const Container = styled.main(({ theme }) => ({
    marginTop: 40,
    padding: '20px 30px 0',
    borderTop: `1px solid ${theme.palette.gray}`
}))

const UserDetails = styled.div(() => ({
    display: 'flex',
    alignItems: 'center'
}))

const UserImage = styled.img(() => ({
    borderRadius: '50%',
    width: 50,
    height: 50,
    objectFit: 'cover'
}))

const UserName = styled.p(() => ({
    fontSize: '16px',
    fontWeight: 600,
    marginLeft: '15px'
}))

const UserDescription = styled.p(() => ({
    fontSize: 12
}))

