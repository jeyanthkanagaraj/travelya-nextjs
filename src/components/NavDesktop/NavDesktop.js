import styled from '@emotion/styled'
import { useRouter } from 'next/router'
import React from 'react'
import Link from 'components/Link/Link'

const NavDesktop = () => {
    const { pathname } = useRouter()

    return (
        <Nav>
            <Link href="/">
                <LogoText>Travelaya</LogoText>
            </Link>
            <List>
                <li>
                    <Link href="/">
                        <NavText active={pathname === '/'}>
                            Flight Deals
                        </NavText>
                    </Link>
                </li>
                <li>
                    <Link href="/hotels">
                        <NavText active={pathname === '/hotels'}>
                            Search Hotels
                        </NavText>
                    </Link>
                </li>
                <li>
                    <Link href="/blog">
                        <NavText active={pathname === '/blog'}>
                            Blog
                        </NavText>
                    </Link>
                </li>
                <li>
                    <Link href="/support">
                        <NavText active={pathname === '/support'}>
                            Support
                        </NavText>
                    </Link>
                </li>
            </List>
            <Link href="/account">
                <NavText active={pathname === '/account'}>
                    Account
                </NavText>
            </Link>
        </Nav>
    )
}

export default NavDesktop

const Nav = styled.nav(({ theme }) => ({
    display: 'none',
    maxWidth: theme.ruler.header.width,
    margin: '0 auto',
    padding: '2.5rem',
    justifyContent: 'space-between',
    alignItems: 'center',
    '& > *': { fontSize: '1.3rem' },
    [theme.mq.medium]: {
        display: 'flex',
    }
}))

const LogoText = styled.div(({ theme }) => ({
    color: theme.palette.secondary,
    fontSize: '2.5rem',
    fontWeight: 'bold',
    textTransform: 'lowercase',
    transition: 'all .2s',
    '&:hover': {
        color: theme.palette.primary.main,
    }
}))

const List = styled.ul(() => ({
    listStyle: 'none',
    display: 'flex',
    width: '40rem',
    justifyContent: 'space-between',
    margin: 0,
    padding: 0,
}))

const NavText = styled.div(({ theme, active = false }) => ({
    color: theme.palette.gray,
    borderBottom: active ? `.1rem solid ${theme.palette.primary.main}` : 'none',
    fontSize: '1.5rem',
    lineHeight: '1.4',
    transition: 'all .2s',
    '&:hover': {
        color: theme.palette.primary.main,
    }
}))
